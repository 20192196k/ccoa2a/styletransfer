package pe.uni.aliquel.styletransfer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {
    ImageView style_image, original_image;
    Bitmap bitmap_style, bitmap_original_image;
    TextView titulo_view;
    Button button_save, button_exit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = getIntent();

        /*Charge Image*/
        String nameImage = intent.getStringExtra("nameImage");
        String imgDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString()+ "/"+nameImage;
        Uri myUri = Uri.parse(imgDir);

        titulo_view = findViewById(R.id.tittle_main);
        /* GetIntent*/

        int style = intent.getIntExtra("Style", 0);
        String name_style = intent.getStringExtra("nameStyle");
        titulo_view.setText(getString(R.string.titulo_main_style, name_style));

        /* Style image */
        style_image = findViewById(R.id.style_img);
        style_image.setImageResource(style);

        BitmapDrawable style_imageDrawable_img_drawable = (BitmapDrawable) style_image.getDrawable();
        bitmap_style = style_imageDrawable_img_drawable.getBitmap();
        /* original image */
        original_image = findViewById(R.id.original_img);
        original_image.setImageURI(myUri);
        BitmapDrawable original_imageDrawable_img_drawable = (BitmapDrawable) original_image.getDrawable();
        bitmap_original_image = original_imageDrawable_img_drawable.getBitmap();

        StyleTransfer styleTransfer = new StyleTransfer(this);
        styleTransfer.inferenceModel(original_image, bitmap_style, bitmap_original_image);

        /* Button save */

        ImageManage imageManage = new ImageManage();
        button_save = findViewById(R.id.button_save);
        button_save.setOnClickListener(v-> {
            String fileName =  System.currentTimeMillis()+".jpg";
            BitmapDrawable bitmapimage = (BitmapDrawable) original_image.getDrawable();
            imageManage.saveImage(this, fileName, bitmapimage);
        });
        button_exit = findViewById(R.id.salir);
        button_exit.setOnClickListener(v->{
            moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
        });
    }

}