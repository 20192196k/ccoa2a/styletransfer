package pe.uni.aliquel.styletransfer;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.ImageView;

import org.tensorflow.lite.support.image.TensorImage;
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer;

import java.io.IOException;

import pe.uni.aliquel.styletransfer.ml.MagentaArbitraryImageStylizationV1256Fp16Prediction1;
import pe.uni.aliquel.styletransfer.ml.MagentaArbitraryImageStylizationV1256Fp16Transfer1;

public class StyleTransfer {
    Context context;
    public StyleTransfer(Context context) {
        this.context = context;
    }

    public void inferenceModel(ImageView original_image, Bitmap style_bitmap, Bitmap content_Bitmap){
        try {
            /*convertir a tensor*/
            MagentaArbitraryImageStylizationV1256Fp16Prediction1 predictionModel = MagentaArbitraryImageStylizationV1256Fp16Prediction1.newInstance(this.context);
            TensorImage styleTensor = TensorImage.fromBitmap(style_bitmap);
            /* predicción style*/
            MagentaArbitraryImageStylizationV1256Fp16Prediction1.Outputs outputs = predictionModel.process(styleTensor);
            TensorBuffer styleBottleneck = outputs.getStyleBottleneckAsTensorBuffer();

            /*Convertir la imágen original a un tensor*/
            TensorImage contentTensor = TensorImage.fromBitmap(content_Bitmap);
            MagentaArbitraryImageStylizationV1256Fp16Transfer1 transferModel = MagentaArbitraryImageStylizationV1256Fp16Transfer1.newInstance(this.context);

            /*Uniendo los dos tensores en el modelo*/
            MagentaArbitraryImageStylizationV1256Fp16Transfer1.Outputs transferOutputs = transferModel.process(contentTensor, styleBottleneck);
            TensorImage styledTensor = transferOutputs.getStyledImageAsTensorImage();
            Bitmap styledBitmap = styledTensor.getBitmap();

            original_image.setImageBitmap(styledBitmap);

            predictionModel.close();
            transferModel.close();
        } catch (IOException e) {
                Log.e("StyleTransfer", e.toString());
        }
    }
}
