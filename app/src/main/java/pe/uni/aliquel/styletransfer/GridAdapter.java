package pe.uni.aliquel.styletransfer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class GridAdapter  extends BaseAdapter {
    Context context;
    ArrayList<String> titulo;
    ArrayList<Integer> image;

    public GridAdapter(Context context, ArrayList<String> titulo, ArrayList<Integer> image) {
        this.context = context;
        this.titulo = titulo;
        this.image = image;
    }

    @Override
    public int getCount() {
        return titulo.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        @SuppressLint("ViewHolder") View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_view_style_layout, parent, false);
        ImageView imageView = view.findViewById(R.id.img_view_style);
        TextView textView = view.findViewById(R.id.text_view_style);
        imageView.setImageResource(image.get(position));
        textView.setText(titulo.get(position));
        return view;
    }

}
