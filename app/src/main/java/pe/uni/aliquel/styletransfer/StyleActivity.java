package pe.uni.aliquel.styletransfer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.GridView;

import java.util.ArrayList;

public class StyleActivity extends AppCompatActivity {
    GridView gridview;
    ArrayList<String> text = new ArrayList<>();
    ArrayList<Integer> style = new ArrayList<>();
    String nameImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        nameImage = intent.getStringExtra("imageName");
        setContentView(R.layout.activity_style);
        gridview = findViewById(R.id.grid_view_style_layout);
        GridAdapter gridAdapter = new GridAdapter(this, text, style);
        gridview.setAdapter(gridAdapter);
        gridview.setOnItemClickListener((parent, view, position, id) -> {
                Intent intent2 = new Intent(StyleActivity.this, MainActivity.class);
                intent2.putExtra("Style", style.get(position));
                intent2.putExtra("nameStyle", text.get(position));
                intent2.putExtra("nameImage", nameImage);
                startActivity(intent2);
            }
        );
        fillArray();
    }

    private void fillArray() {
        text.add("Kanagawa");
        text.add("Nightmark");
        text.add("Udnie");
        text.add("Mosaic");
        text.add("candy");
        text.add("The Scream");
        text.add("Picasso");
        text.add("Rain princess");
        text.add("Rlon wang");
        style.add(R.drawable.kanagawa);
        style.add(R.drawable.nightmark);
        style.add(R.drawable.udnie);
        style.add(R.drawable.mosaic);
        style.add(R.drawable.candy);
        style.add(R.drawable.thescream);
        style.add(R.drawable.picasso);
        style.add(R.drawable.rainprincess);
        style.add(R.drawable.rlonwang);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(StyleActivity.this, GaleryActivity.class);
        startActivity(intent);
        finish();
    }
}