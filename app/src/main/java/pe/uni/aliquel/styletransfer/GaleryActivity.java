package pe.uni.aliquel.styletransfer;


import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
//import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
//import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;


public class GaleryActivity extends AppCompatActivity {
    ImageView imageView;
    String nameImage;
    Button button_give_img_galeria, button_send_img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_galery);
        imageView = findViewById(R.id.galery_img);
        button_give_img_galeria = findViewById(R.id.button_select_img_galeria);
        button_send_img = findViewById(R.id.button_send_img);
        nameImage = "input.jpg";
        if (ContextCompat.checkSelfPermission(
                this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                    this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1
            );

        }
        ImageManage imageMange = new ImageManage();
        //button_give_img_camara.setOnClickListener(v->);
        button_give_img_galeria.setOnClickListener(v -> imageMange.chargeGaleryImage(launcher_galeria));

        button_send_img.setOnClickListener(v-> {
                Intent intent = new Intent(GaleryActivity.this, StyleActivity.class);
                BitmapDrawable bitmapimageView = (BitmapDrawable) imageView.getDrawable();
                imageMange.saveImage(this, nameImage, bitmapimageView);
                intent.putExtra("imageName", nameImage);
                startActivity(intent);
        });
    }


    private final ActivityResultLauncher<Intent> launcher_galeria = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK
                        && result.getData() != null) {
                    Uri photoUri = result.getData().getData();
                    String mimeType = getContentResolver().getType(photoUri);

                    if(mimeType.endsWith("jpeg") || mimeType.endsWith("png") || mimeType.endsWith("jpg")){
                        imageView.setImageURI(photoUri);
                        button_send_img.setVisibility(View.VISIBLE);
                        button_give_img_galeria.setVisibility(View.GONE);
                    }else{
                        Toast.makeText(this, R.string.adv_type_img , Toast.LENGTH_LONG).show();
                    }

                }
            }
    );

}