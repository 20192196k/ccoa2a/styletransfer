package pe.uni.aliquel.styletransfer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public final class ImageManage {
    public ImageManage() {
    }


    public void chargeGaleryImage(ActivityResultLauncher<Intent>  launcher){
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        launcher.launch(intent);
    }

    //String fileName =  System.currentTimeMillis()+".jpg";
    public void saveImage(Context context, String fileName, BitmapDrawable imageView) {
        //BitmapDrawable imageView = (BitmapDrawable)  this.imageView.getDrawable();
        OutputStream outputStream = null;
        File file;
        String imageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath();
        file = new File(imageDir);
        boolean isDirectoryCreated = file.exists();
        if (!isDirectoryCreated) {
            isDirectoryCreated = file.mkdir();
        }
        if (isDirectoryCreated) {
            imageDir = imageDir +"/"+fileName;
            file = new File(imageDir);
            try {
                outputStream = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            Bitmap bitImage = imageView.getBitmap();
            boolean saved = bitImage.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            if (saved) {
                Toast.makeText(context, R.string.saved_image, Toast.LENGTH_LONG).show();
            }
            if (outputStream != null) {
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            MediaScannerConnection.scanFile(context, new String[]{file.toString()}, null, null);

        }
    }
}
