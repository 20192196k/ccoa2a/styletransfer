package pe.uni.aliquel.styletransfer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.CountDownTimer;
import android.view.animation.AnimationUtils;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.animation.Animation;
import android.widget.ImageView;

@SuppressLint("CustomSplashScreen")
public class IntroActivity extends AppCompatActivity {
    ImageView imageLogo;
    Animation animationImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        imageLogo = findViewById(R.id.logo);
        animationImage = AnimationUtils.loadAnimation(this, R.anim.img_animation);
        imageLogo.setAnimation(animationImage);

        new CountDownTimer(4000, 1000){
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                Intent intent = new Intent(IntroActivity.this,  GaleryActivity.class);
                startActivity(intent);
                finish();
            }
        }.start();
    }
}